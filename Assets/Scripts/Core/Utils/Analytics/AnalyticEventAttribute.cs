using System;

namespace Core.Utils.Analytics
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Struct | AttributeTargets.Method | AttributeTargets.Property)]
    public class AnalyticEventAttribute : Attribute
    {
        
        public string Key { get; }
        
        public string SendEventConditionFieldName { get; }
        
        public string[] Params { get; }

        public AnalyticEventAttribute(string key, string sendEventConditionFieldName = null)
        {
            Key = key;
            SendEventConditionFieldName = sendEventConditionFieldName;
        }

        public AnalyticEventAttribute(string key, params string[] @params)
        {
            Key = key;
            Params = @params;
        }
    }
}