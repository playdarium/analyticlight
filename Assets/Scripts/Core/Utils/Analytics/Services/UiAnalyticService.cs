using System;
using System.Collections.Generic;
using System.Reflection;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Core.Utils.Analytics.Services
{
    public class UiAnalyticService : IDisposable, IInitializable
    {
        private readonly IList<IAnalyticable> _analyticables;

        private readonly CompositeDisposable _disposables = new CompositeDisposable();

        public UiAnalyticService(IList<IAnalyticable> analyticables, SignalBus signalBus)
        {
            _analyticables = analyticables;
        }

        public void Initialize()
        {
            foreach (var analyticable in _analyticables) ProcessAnalyticable(analyticable);

        }
        
        public void Dispose()
        {
            _disposables?.Dispose();
        }
        
        private void ProcessAnalyticable(IAnalyticable analyticable)
        {
            var fields = analyticable.GetType().GetFields();
            foreach (var fieldInfo in fields)
            {
                TryProcessUiEvent(fieldInfo, analyticable);

            }
        }

        private bool TryProcessUiEvent(FieldInfo fieldInfo, IAnalyticable analyticable)
        {
            var attr = fieldInfo.GetCustomAttribute<AnalyticEventAttribute>();
            if (attr == null) return false;
            var button = fieldInfo.GetValue(analyticable) as Button;
            if (button != null)
            {
                button.OnClickAsObservable().Subscribe(_ =>
                {
                    SendClickEvent(attr.Key);
                }).AddTo(button);
                return true;
            }
            
            var toggle = fieldInfo.GetValue(analyticable) as Toggle;
            if (toggle != null)
            {
                toggle.OnValueChangedAsObservable().Subscribe(_ =>
                {
                    SendToggleEvent(attr.Key, toggle.isOn);
                }).AddTo(toggle);
                return true;
            }
            throw new ArgumentException("Ui event attribute can be applied only to UI.Button, UI.Toggle, localizable: " +
                                            analyticable.GetType().Name);

        }

        private void SendClickEvent(string key)
        {
            Debug.Log("Send ui event: " + key);
        }
        
        private void SendToggleEvent(string key, bool value)
        {
            Debug.Log("Send ui event: " + key + ", value: " + value);
        }
    }
}