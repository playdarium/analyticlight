using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Remoting.Messaging;
using UniRx;
using UnityEngine;
using Zenject;

namespace Core.Utils.Analytics.Services
{
    public class AnalyticsService<T> : IInitializable, IDisposable where T : ISignalAnalytic
    {
        private readonly SignalBus _signalBus;
        
        private readonly CompositeDisposable _disposables = new CompositeDisposable();

        private readonly IDictionary<string, object> _paramsBuffer = new Dictionary<string, object>();
        
        public AnalyticsService(SignalBus signalBus)
        {
            _signalBus = signalBus;
        }
        
        //===============  Signals ===============================================
        
        public void Initialize()
        {
#if ANALYTIC_LIGHT_ADD_ADVICE            
            _signalBus.GetStream<SignalAdviceAfter>().Subscribe(s => OnSignalAdviceAfter(s.MethodMessage)).AddTo(_disposables);
#endif
            var assembly = Assembly.GetExecutingAssembly();
            var signalAnalyticsTypes = assembly.GetTypes().Where(t => !t.IsInterface && typeof(T).IsAssignableFrom(t));
            foreach (var signalAnalyticsType in signalAnalyticsTypes)
            {
                _signalBus.GetStream(signalAnalyticsType).Subscribe(HandleSignal).AddTo(_disposables);
            }
        }

        public void Dispose()
        {
            _disposables?.Clear();
        }

        private void HandleSignal(object signal)
        {
            var type = signal.GetType();
            var mainAttr = type.GetCustomAttribute<AnalyticEventAttribute>();
            if(mainAttr == null)
                return;

            var sendConditionField = mainAttr.SendEventConditionFieldName;
            if (!string.IsNullOrEmpty(sendConditionField))
            {
                var field = type.GetField(sendConditionField);
                var sendEvent = field.GetValue(signal);
                if (sendEvent is bool)
                {
                    if(!(bool)sendEvent)
                        return;
                }
                else
                {
                    throw new ArgumentException("SendEventConditionFieldName should be applied only to boolean fields"); 
                }
            }

            var eventName = mainAttr.Key;
            
            _paramsBuffer.Clear();
            var fields = type.GetFields();
            foreach (var field in fields)
            {
                var attr = field.GetCustomAttribute<AnalyticEventAttribute>();
                if(attr == null) continue;
                
                var data = field.GetValue(signal);
                _paramsBuffer.Add(attr.Key, data);
            }
            SendGameEvent(eventName, _paramsBuffer);
        }
        
        
        
        //===============  Advice ===============================================
        
        private void OnSignalAdviceAfter(IMethodMessage methodMessage)
        {
            ProcessGameEvent(methodMessage);
        }
        
        private void ProcessGameEvent(IMethodMessage methodMessage)
        {
            var attr = methodMessage.MethodBase.GetCustomAttribute<AnalyticEventAttribute>();
            if(attr == null) return;
            if (attr.Params == null)
            {
                SendGameEvent(attr.Key);
                return;
            }
            
            if(attr.Params.Length != methodMessage.ArgCount) 
                throw new ArgumentException("params count should be equals ArgCount");

            _paramsBuffer.Clear();
            for (var i = 0; i < attr.Params.Length; i++)
            {
                _paramsBuffer.Add(attr.Params[i], methodMessage.Args[i]);   
            }
            SendGameEvent(attr.Key, _paramsBuffer);

        }

        private void SendGameEvent(string name, IDictionary<string, object> @params = null)
        {
            Debug.Log("Send game event: " + name);
            if(@params == null) return;
            foreach (var param in @params)
            {
                Debug.Log("Param, key: " + param.Key + ", value: " + param.Value);
            }
        }
    }
}